package seabattle

fun main() {
    val ships = 3
    val oneDeck = 2
    val twoDeck = 1
    println("Игрок 1 представьтесь")
    val name1 = readLine()!!
    val player1 = Player(name1)
    var allShips = ships
    val playerFields1: Array<Array<Char>> = Array(4) { Array(4) { '-' } }
    while (allShips > 0) {
        println("Введите количество палуб в корабле (можно выбрать всего однопалубных: $oneDeck и двупалубных: $twoDeck)")
        val howMuch = readLine()!!.toInt()
        println("Позиция по горизонтали до 4")
        val x = readLine()!!.toInt()
        println("Позиция по вертикали до 4")
        val y = readLine()!!.toInt()
        println("Введите 1 если хотите расположить корабль по горизонтали, введите 2 - если по вертикали")
        val position = readLine()!!.toInt()
        val ship = Ship(x, y, position, howMuch)
        if (player1.validate(playerFields1, ship)) {
            player1.fillField(playerFields1, ship)
            allShips -= 1
        } else println("Нельзя поставить корабль по таким координатам")
    }
    println("Игрок 2 представьтесь")
    val name2 = readLine()!!
    val player2 = Player(name2)
    var allShips2 = ships
    val playerFields2: Array<Array<Char>> = Array(4) { Array(4) { '-' } }
    while (allShips2 > 0) {
        println("Введите количество палуб в корабле (можно выбрать всего однопалубных: $oneDeck и двупалубных: $twoDeck)")
        val howMuch1 = readLine()!!.toInt()
        println("Позиция по горизонтали")
        val x1 = readLine()!!.toInt()
        println("Позиция по вертикали")
        val y1 = readLine()!!.toInt()
        println("Введите 1 если хотите расположить корабль по горизонтали, введите 2 - если по вертикали")
        val position1 = readLine()!!.toInt()
        val ship = Ship(x1, y1, position1, howMuch1)
        if (player2.validate(playerFields2, ship)) {
            player2.fillField(playerFields2, ship)
            allShips2 -= 1
        } else println("Нельзя поставить корабль по таким координатам")
    }
    playGame(player1, player2, playerFields1, playerFields2)
}

class Player(val name: String) {
    fun fillField(fields: Array<Array<Char>>, ship: Ship) {
        if (ship.position == 1) {
            for (i in 0 until ship.howMuch) {
                fields[ship.y - 1][ship.x - 1 + i] = '1'
            }
        }
        if (ship.position == 2) {
            for (i in 0 until ship.howMuch) {
                fields[ship.y - 1 + i][ship.x - 1] = '1'
            }
        }
        for (array in fields) {
            for (value in array) {
                print("$value ")
            }
            println()
        }
    }

    fun validate(fields: Array<Array<Char>>, ship: Ship): Boolean {
        if (ship.position == 1) {
            if (ship.x - 1 + ship.howMuch > fields.size) {
                return false
            }
        }
        if (ship.position == 2) {
            if (ship.y - 1 + ship.howMuch > fields.size) {
                return false
            }
        }
        var deck = ship.howMuch
        while (deck > 0) {
            for (i in 0 until ship.howMuch) {
                var xi = 0
                var yi = 0
                if (ship.position == 1) {
                    xi = i
                } else {
                    yi = i
                }
                if (ship.x + xi < fields.size && ship.x + xi >= 0) {
                    if (fields[ship.x + xi][ship.y - 1 + yi] == '1') {
                        return false
                    }
                }
                if (ship.x - 1 + xi < fields.size && ship.x - 1 + xi >= 0) {
                    if (fields[ship.x - 1 + xi][ship.y - 1 + yi] == '1') {
                        return false
                    }
                }
                if (ship.x - 2 + xi < fields.size && ship.x - 2 + xi >= 0) {
                    if (fields[ship.x - 2 + xi][ship.y - 1 + yi] == '1') {
                        return false
                    }
                }
                if (ship.y + yi < fields.size && ship.y + yi >= 0) {
                    if (fields[ship.y + yi][ship.x - 1 + xi] == '1') {
                        return false
                    }
                }
                if (ship.y - 1 + yi < fields.size && ship.y - 1 + yi >= 0) {
                    if (fields[ship.y - 1 + yi][ship.x - 1 + xi] == '1') {
                        return false
                    }
                }
                if (ship.y - 2 + yi < fields.size && ship.y - 2 + yi >= 0) {
                    if (fields[ship.y - 2 + yi][ship.x - 1 + xi] == '1') {
                        return false
                    }
                }
            }
            deck--
        }
        return true
    }
}

class Ship(val x: Int, val y: Int, val position: Int, val howMuch: Int)

fun playGame(
    player1: Player,
    player2: Player,
    playerFields1: Array<Array<Char>>,
    playerFields2: Array<Array<Char>>
) {
    val player1FieldsWhereShot: Array<Array<Char>> = Array(4) { Array(4) { '-' } }
    val player2FieldsWhereShot: Array<Array<Char>> = Array(4) { Array(4) { '-' } }
    var currentPlayer = player1
    var currentPlayerField = playerFields2
    var currentPlayerFieldsWhereShot = player1FieldsWhereShot
    while (isPlayerAlive(playerFields1) && isPlayerAlive(playerFields2)) {
        println("${currentPlayer.name} посмотрите куда вы стреляли,где # - Попадание, * - Промах, :")
        for (array in currentPlayerFieldsWhereShot) {
            for (value in array) {
                print("$value")
            }
            println()
        }
        println("${currentPlayer.name} введите координату выстрела по горизонтали")
        val shotX = readLine()!!.toInt()
        println("${currentPlayer.name} введите координату выстрела по вертикали")
        val shotY = readLine()!!.toInt()
        var shotResult = isShotGood(currentPlayerFieldsWhereShot, currentPlayerField, shotX, shotY)
        if (shotResult == 0 && currentPlayer.name == player2.name) {
            currentPlayer = player1
            currentPlayerField = playerFields2
            currentPlayerFieldsWhereShot = player1FieldsWhereShot
            shotResult = 1
        }
        if (shotResult == 0 && currentPlayer.name == player1.name) {
            currentPlayer = player2
            currentPlayerField = playerFields1
            currentPlayerFieldsWhereShot = player2FieldsWhereShot
        }
    }
    println("Игорок:${currentPlayer.name} выиграл. Поздравляем!")
}

fun isPlayerAlive(fields: Array<Array<Char>>): Boolean {
    for (array in fields) {
        for (value in array) {
            if (value == '1') {
                return true
            }
        }
    }
    return false
}

private fun isShotGood(playerFieldsWhereShot: Array<Array<Char>>, fields: Array<Array<Char>>, x: Int, y: Int): Int {
    return if (fields[y - 1][x - 1] == '1') {
        fields[y - 1][x - 1] = '#'
        playerFieldsWhereShot[y - 1][x - 1] = '#'
        println("Хороший выстрел!")
        1
    } else {
        playerFieldsWhereShot[y - 1][x - 1] = '*'
        println("Промах!")
        0
    }
}
