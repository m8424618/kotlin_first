package homework6

fun main() {
    println("Задача 1. Применение принципов ООП")
    val lion = Lion("Алекс", 7, 8)
    lion.eat(arrayOf("Кабан", "Трава"))

    val tiger = Tiger("Тигра", 7, 5)
    tiger.eat(arrayOf("Зебра", "Куст"))

    val wolf = Wolf("Серый", 8, 66)
    wolf.eat(arrayOf("Олень", "Пиво", "Кабан"))

    val hippo = Hippo("Глория", 66, 350)
    hippo.eat(arrayOf("Трава", "Мясо"))

    val elephant = Elephant("Слоник", 88, 300)
    elephant.eat(arrayOf("Овощи", "Сено"))

    val giraffe = Giraffe("Мелман", 17, 55)
    giraffe.eat(arrayOf("Акация", "Слон"))

    val chimpanzee = Chimpanzee("Саша", 6, 7)
    chimpanzee.eat(arrayOf("Фрукты", "Зебра"))

    val gorilla = Gorilla("Горилла", 77, 77)
    gorilla.eat(arrayOf("Мясо", "Фрукты"))

    println("Задача 2. Применение полиморфизма")
    val animals: Array<Animal> = arrayOf(lion, tiger, hippo, wolf, giraffe, gorilla, elephant, chimpanzee)
    val foods = arrayOf("Кабан", "Фрукты", "Зебра", "Трава", "Чипсы")
    animalEat(animals, foods)
}

abstract class Animal(open val name: String, open val height: Int, open val weight: Int) {
    abstract var satiety: Int
    abstract val food: Array<String>
    abstract fun eat(products: Array<String>)
}

abstract class Herbivores(override val name: String, override val height: Int, override val weight: Int) :
    Animal(name, height, weight) {
    override var satiety = 0
    abstract override val food: Array<String>
    override fun eat(products: Array<String>) {
        for (product in products) {
            if (product in food) {
                satiety++
                println("Травоядное животное по имени $name покушал!")
            } else println("Травоядное животное по имени $name не ест $product")
        }
    }
}

abstract class Predactors(override val name: String, override val height: Int, override val weight: Int) :
    Animal(name, height, weight) {
    override var satiety = 0
    abstract override val food: Array<String>
    override fun eat(products: Array<String>) {
        for (product in products) {
            if (product in food) {
                satiety++
                println("Хищное животное по имени $name покушал!")
            } else println("Хищное животное по имени $name не ест $product")
        }
    }
}

class Lion(override val name: String, override val height: Int, override val weight: Int) :
    Predactors(name, height, weight) {
    override val food: Array<String> = arrayOf("Кабан", "Зебра", "Мясо")
}

class Tiger(override val name: String, override val height: Int, override val weight: Int) :
    Predactors(name, height, weight) {
    override val food = arrayOf("Олень", "Буйвол", "Кабан")
}

class Wolf(override val name: String, override val height: Int, override val weight: Int) :
    Predactors(name, height, weight) {
    override val food: Array<String> = arrayOf("Лось", "Олень", "Кабан")
}

class Hippo(override val name: String, override val height: Int, override val weight: Int) :
    Herbivores(name, height, weight) {
    override val food: Array<String> = arrayOf("Трава", "Трава около воды")
}

class Giraffe(override val name: String, override val height: Int, override val weight: Int) :
    Herbivores(name, height, weight) {
    override val food: Array<String> = arrayOf("Трава", "Листья", "Акация")
}

class Chimpanzee(override val name: String, override val height: Int, override val weight: Int) :
    Herbivores(name, height, weight) {
    override val food: Array<String> = arrayOf("Фрукты", "Насекомые", "Листья")
}

class Elephant(override val name: String, override val height: Int, override val weight: Int) :
    Herbivores(name, height, weight) {
    override val food: Array<String> = arrayOf("Трава", "Сено", "Овощи")
}

class Gorilla(override val name: String, override val height: Int, override val weight: Int) :
    Herbivores(name, height, weight) {
    override val food: Array<String> = arrayOf("Фрукты", "Насекомые", "Листья")
}

fun animalEat(animals: Array<Animal>, products: Array<String>) {
    for (animal in animals) {
        animal.eat(products)
    }
}

