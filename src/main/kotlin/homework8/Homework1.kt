package homework8

fun main() {
    val sourceList = mutableListOf(1, 2, 3, 1, 2, 3, 1, 1, 5, 6, 8, 1, 8, 7, 4, 9)
    println(makeUnique(sourceList))
}

fun makeUnique(listBefore: List<Int>): List<Int> {
    return listBefore.toSet().toList() //есть функция distinct()
}
