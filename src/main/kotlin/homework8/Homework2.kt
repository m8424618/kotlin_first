package homework8

val userCart = mutableMapOf<String, Int>("potato" to 2, "cereal" to 2, "milk" to 1, "sugar" to 3, "onion" to 1, "tomato" to 2, "cucumber" to 2, "bread" to 3)
val discountSet = setOf("milk", "bread", "sugar")
val discountValue = 0.20
val vegetableSet = setOf("potato", "tomato", "onion", "cucumber")
val prices = mutableMapOf<String, Double>(
    "potato" to 33.0,
    "sugar" to 67.5,
    "milk" to 58.7,
    "cereal" to 78.4,
    "onion" to 23.76,
    "tomato" to 88.0,
    "cucumber" to 68.4,
    "bread" to 22.0
)

fun main() {
    println(countOfVegetables(userCart, vegetableSet))
    println(sumOfProducts(userCart, prices, discountSet, discountValue))
}
fun countOfVegetables(maps: Map<String,Int>, sets: Set<String>): Int {
    var result = 0
    for (set in sets) {
        result += maps.getValue(set)
    }
    return result
}

fun sumOfProducts(products: Map<String, Int>, prices: Map<String, Double>, discountProduct: Set<String>, discount: Double) : Double{
    var resultPrice = 0.0
    for (product in products) {
        resultPrice += if (product.key in discountProduct) {
            prices.getValue(product.key)*products.getValue(product.key)*(1-discount)
        } else prices.getValue(product.key)*products.getValue(product.key)
    }

    return resultPrice
}

