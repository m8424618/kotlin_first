val sum = { a: Double, b: Double -> a + b }
val subtraction = { a: Double, b: Double -> a - b }
val division = {a: Double, b: Double -> if(b!=0.0)a/b else throw Exception("На ноль делить нельзя")}
val multiplication = {a: Double, b: Double -> a*b}

fun main() {
    calculator(getCalculationMethod("+"), 2.0, 3.0)
    calculator(getCalculationMethod("-"), 2.0, 13.0)
    calculator(getCalculationMethod("*"), 2.0, 13.0)
    calculator(getCalculationMethod("/"), 26.0, 13.0)
    calculator(getCalculationMethod("/"), 0.0, 13.0)
    calculator(getCalculationMethod("/"), 26.0, 0.0)

}

fun calculator(lambda : ((Double, Double)-> Double), a: Double, b: Double ) {
    println(lambda(a,b))
}

fun getCalculationMethod(name: String) :(Double, Double)-> Double {
    return when (name) {
        "+" -> sum
        "-" -> subtraction
        "*" -> multiplication
        "/" -> division
        else -> throw UnsupportedOperationException()
    }
}