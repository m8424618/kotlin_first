package homework7

fun main() {
    println("Введите логин")
    val login = readLine()!!
    println("Введите пароль")
    val password = readLine()!!
    println("Введите пароль еще раз")
    val password2 = readLine()!!
    checkLoginAndPass(login,password,password2)
}
class User(val login: String, val password: String)
class WrongLoginException(message: String) : Exception(message)
class WrongPasswordException(message: String) : Exception(message)
fun checkLoginAndPass(login: String, password: String, password2: String): User {
    if (login.length in 1..20) println("Введен правильный логин") else throw WrongLoginException("Введен неправильный логин")
    if (password.length>=10) println("Введен правильный пароль") else throw WrongPasswordException("Введен неправильный пароль")
    if (password == password2) println("Пароли совпадают") else throw WrongPasswordException("Пароль и подтверждение пароля не совпадают")

    return User(login,password)
}
