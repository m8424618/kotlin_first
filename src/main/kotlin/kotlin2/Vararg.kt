package kotlin2

fun main() {
demoVararg("12","122","1234", "fpo")
}

fun demoVararg(vararg params: String) {
    var howManyParams = 0
    var totalString = ""
    for (p in params) {
        howManyParams += 1
        totalString +="$p;"
    }
    println("Передано $howManyParams элемента:")
    println(totalString)
}