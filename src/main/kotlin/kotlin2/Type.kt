package kotlin2

import java.time.LocalDate
import java.util.logging.Handler
import kotlin.math.roundToInt

fun checkType(any: Any?) {
    when {
        any is String -> println("Я получил тип String = $any, ее длина равна ${any.length}")
        any is Int -> println("Я получил Int = $any, его квадрат равен ${any*any}")
        any is Double -> println("Я получил Double = $any, это число округляется до ${(any*100).roundToInt()/100.0}")
        any is LocalDate && any > LocalDate.of(2006,12,24) -> println("Я получил LocalData = $any, эта дата меньше чем дата основания Tinkoff")
        any is LocalDate && any < LocalDate.of(2006,12,24) -> println("Я получил LocalData = $any, эта дата больше чем дата основания Tinkoff")
        any == null -> println("Объект равен null")
        else -> println("Мне этот тип неизвестен(")
    }
}

fun main() {
    checkType("Privet")
    checkType(145)
    checkType(145.0)
    checkType(145.2817812)
    checkType(LocalDate.of(1990,1,1))
    checkType(LocalDate.of(2013,1,1))
    checkType(null)
    checkType(Handler::class)
}