package kotlin2

fun main() {
createPerson(name = "Ваня", surname = "Иванов", gender = "Муж", age = 43, dateOfBirth = "1980-01-04")
createPerson(name = "Анна", surname = "Иванова", patronymic = "Петровна", gender = "Жен", age = 63,
    dateOfBirth = "1960-01-30", inn = "1234567890", snils = "111-111-111")
createPerson(surname = "Иванов", age = 43, name = "Ваня",snils = "111-111-111", gender = "Муж", dateOfBirth = "1980-01-04")
}

fun createPerson(name: String, surname : String, patronymic : String? = "-", gender: String, age: Int,
                 dateOfBirth: String, inn: String? = "-", snils: String? = "-" ) {
println("Имя: $name, Фамилия: $surname , Отчество(необязательно): $patronymic, Пол: $gender, Возраст: $age, Дата рождения: $dateOfBirth," +
        " ИНН(необязательно): $inn , СНИЛС(необязательно): $snils")
}