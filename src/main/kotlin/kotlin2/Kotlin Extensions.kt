package kotlin2

fun main() {
    val demoList = mutableListOf<Int>(1, 4, 9, 16, 25)
    demoList.squared()
    println(demoList)
}

fun MutableList<Int>.squared() {
    for (i in this.indices) {
        this[i] = this[i]*this[i]
    }
}
