package kotlin2

fun main() {
    println(createPerson(name = "Анна", surname = "Иванова", patronymic = "Петровна", gender = "Жен", age = 63,
        dateOfBirth = "1960-01-30", inn = "1234567890", snils = "111-111-111"))
    println(createPerson(name = "Ваня", surname = "Иванов", gender = "Муж", age = 19,
        dateOfBirth = "1960-01-30"))
}
data class Person(val name: String, val surname : String, val patronymic : String? = null, val gender: String, val age: Int,
                  val dateOfBirth: String, val inn: String? = null, val snils: String? = null)

fun createPerson(
    name: String, surname: String, patronymic: String? = null, gender: String, age: Int,
    dateOfBirth: String, inn: String? = null, snils: String? = null
): Person {
    return Person(name, surname, patronymic, gender, age, dateOfBirth, inn, snils)
}
