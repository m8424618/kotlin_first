package kotlin2

class DemoForCompanion {
    companion object MyCompanion {
        var counter = 0
        fun printMessage() {
            counter += 1
            println("Вызван counter. Количество вызовов = $counter")
        }
    }
}

fun main() {
    DemoForCompanion.printMessage()
    DemoForCompanion.MyCompanion.printMessage()
    DemoForCompanion.printMessage()
    DemoForCompanion.printMessage()
}