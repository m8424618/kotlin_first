import homeworkGenerics.*

fun main() {
    val tiger = Tiger()
    useRunSkill(tiger)
    useSwimSkill(tiger)
    useSwimAndRunSkill(tiger)

    val lion = Lion()
    useRunSkill(lion)
    useSwimSkill(lion)
    useSwimAndRunSkill(lion)

    val tuna = Tuna()
    //useRunSkill(tuna) тут ошибка, так как не реализовывает Runnable
    useSwimSkill(tuna)
    //useSwimAndRunSkill(tuna) тут ошибка, так как не реализовывает Runnable

    val salmon = Salmon()
    useSwimSkill(salmon)

    val cat = Cat()
    useSwimAndRunSkill(cat)
}

fun <T : Runnable> useRunSkill(pet: T) {
    pet.run()
}

fun <T : Swimmable> useSwimSkill(pet: T) {
    pet.swim()
}
fun <T> useSwimAndRunSkill(pet: T) where T: Runnable , T : Swimmable{
    pet.swim()
    pet.run()
}


