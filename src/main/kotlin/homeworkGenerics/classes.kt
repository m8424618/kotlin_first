package homeworkGenerics


abstract class Pet

open class Cat() : Pet(), Runnable, Swimmable {
    override fun run() {
        println("I am a Cat, and i running")
    }
    override fun swim() {
        println("I am a Cat, and i swimming")
    }
}

open class Fish() : Pet(), Swimmable {
    override fun swim() {
        println("I am a Fish, and i swimming")
    }
}

class Tiger(): Cat() {
    override fun run() {
        println("I am a Tiger, and i running")
    }
    override fun swim() {
        println("I am a Tiger, and i swimming")
    }
}

class Lion(): Cat() {
    override fun run() {
        println("I am a Lion, and i running")
    }
    override fun swim() {
        println("I am a Lion, and i swimming")
    }
}

class Tuna() : Fish() {
    override fun swim() {
        println("I am a Tuna, and i swimming")
    }
}

class Salmon() : Fish() {
    override fun swim() {
        println("I am a Salmon, and i swimming")
    }
}