package homeworkGenerics

interface Runnable {
    fun run()
}
interface Swimmable {
    fun swim()
}