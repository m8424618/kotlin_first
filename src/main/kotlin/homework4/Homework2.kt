package homework4
fun main() {
    val marks = arrayOf(3, 4, 5, 2, 3, 5, 5, 2, 4, 5, 2, 4, 5, 3, 4, 3, 3, 4, 4, 5)
    val result = percentOfMark(marks)
    println("Отличников - ${result[0]}%, Хорошистов - ${result[1]}%, Троечников - ${result[2]}%, Двоечников - ${result[3]}%")
    //в чатике сказали, что не надо округлять до одного знака после запятой, можно сделать так: Отличников - ${"%.1f".format(result[0])}%
}
fun percentOfMark(array: Array<Int>): Array<Double> {
    var markFive = 0.0
    var markFour = 0.0
    var markThree = 0.0
    var markTwo = 0.0
    for (i in array) {
        when(i) {
            5 -> markFive++
            4 -> markFour++
            3 -> markThree++
            2 -> markTwo++
        }
    }
    val percentOfFive = (markFive/array.size)*100
    val percentOfFour = (markFour/array.size) * 100
    val percentOfThree = (markThree/array.size) * 100
    val percentOfTwo = (markTwo/array.size) * 100

    return arrayOf(percentOfFive, percentOfFour, percentOfThree, percentOfTwo)
}
