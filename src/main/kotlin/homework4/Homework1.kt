package homework4

fun main() {
    val myArray = arrayOf(1, 25, 10, 6, 22, 17, 8, 14, 23)
    val moneyAfterSorting: Array<Int> = whatIsCoin(myArray)
    println("Деньги Джо: ${moneyAfterSorting[0]}, деньги команды: ${moneyAfterSorting[1]}")
}

fun whatIsCoin(array: Array<Int>): Array<Int> {
    var coinJo = 0
    var coinTeam = 0
    for(i in array) {
        if (i % 2 == 0) coinJo++
        else coinTeam++
    }
    return arrayOf(coinJo,coinTeam)
}
