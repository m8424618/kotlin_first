package homework5

fun main() {
    print("Введите число:")
    val digit = readLine()!!.toInt()
    println(myReverse(digit))
}
fun myReverse(intToReverse: Int): Int {
    var result = 0
    var a = intToReverse
    while (a > 0) {
        result = result * 10 + a % 10
        a /= 10
    }
    return result
}
