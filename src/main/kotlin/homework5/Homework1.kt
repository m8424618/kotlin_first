package homework5
fun main() {
    val lion = Lion("Лева", 55, 160)
    lion.eat("Чипсы")
    lion.eat("Зебра")

    val tiger = Tiger("Тигра", 15, 77)
    tiger.eat("Олень")

    val hippo = Hippo("Толстяк", 14, 350)
    hippo.eat("Трава")

    val wolf = Wolf("Серый", 7, 44)
    wolf.eat("Лось")

    val giraffe = Giraffe("Пятнистый", 66, 80)
    giraffe.eat("Сникерс")

    val elephant = Elephant("Слоник", 55, 400)
    elephant.eat("Овощи")

    val chimpanzee = Chimpanzee("Шимпанзе", 1, 7)
    chimpanzee.eat("Фрукты")

    val gorilla = Gorilla("Горилла", 6, 66)
    gorilla.eat("Мороженое")
}
class Lion(val name: String, val height: Int, val weight: Int) {
    val food: Array<String> = arrayOf("Зебра","Буйвол","Кабан")
    var satiety = 0
    fun eat (product:String) {
        if (product in food) {
            satiety ++
            println("Лев покушал!, его сытость $satiety")
        }
        else println("Лев не ест $product, его сытость $satiety")
    }
}
class Tiger(val name: String, val height: Int, val weight: Int) {
    val food: Array<String> = arrayOf("Олень","Косуля","Кабан")
    var satiety = 0
    fun eat (product:String) {
        if (product in food) {
            satiety ++
            println("Тигр покушал!, его сытость $satiety")
        }
        else println("Тигр не ест $product, его сытость $satiety")
    }
}
class Hippo(val name: String, val height: Int, val weight: Int) {
    val food: Array<String> = arrayOf("Трава","Трава около воды")
    var satiety = 0
    fun eat (product:String) {
        if (product in food) {
            satiety ++
            println("Бегемот покушал!, его сытость $satiety")
        }
        else println("Бегемот не ест $product, его сытость $satiety")
    }
}

class Wolf(val name: String, val height: Int, val weight: Int) {
    val food: Array<String> = arrayOf("Лось","Олень","Кабан")
    var satiety = 0
    fun eat (product:String) {
        if (product in food) {
            satiety ++
            println("Волк покушал!, его сытость $satiety")
        }
        else println("Волк не ест $product, его сытость $satiety")
    }
}
class Giraffe(val name: String, val height: Int, val weight: Int) {
    val food: Array<String> = arrayOf("Трава","Листья","Акация")
    var satiety = 0
    fun eat (product:String) {
        if (product in food) {
            satiety ++
            println("Жираф покушал!, его сытость $satiety")
        }
        else println("Жираф не ест $product, его сытость $satiety")
    }
}
class Chimpanzee(val name: String, val height: Int, val weight: Int) {
    val food: Array<String> = arrayOf("Фрукты","Насекомые","Листья")
    var satiety = 0
    fun eat (product:String) {
        if (product in food) {
            satiety ++
            println("Шимпанзе покушал!, его сытость $satiety")
        }
        else println("Шимпанзе не ест $product, его сытость $satiety")
    }
}
class Elephant(val name: String, val height: Int, val weight: Int) {
    val food: Array<String> = arrayOf("Трава","Сено","Овощи")
    var satiety = 0
    fun eat (product:String) {
        if (product in food) {
            satiety ++
            println("Слон покушал!, его сытость $satiety")
        }
        else println("Слон не ест $product, его сытость $satiety")
    }
}
class Gorilla(val name: String, val height: Int, val weight: Int) {
    val food: Array<String> = arrayOf("Фрукты","Насекомые","Листья")
    var satiety = 0
    fun eat (product:String) {
        if (product in food) {
            satiety ++
            println("Горилла покушала!, его сытость $satiety")
        }
        else println("Горрила не ест $product, его сытость $satiety")
    }
}
