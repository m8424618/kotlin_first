import kotlin.math.roundToInt

fun main() {
    val list = listOf(13.31, 3.98, 12.0, 2.99, 9.0)
    val list2 = listOf(133.21, null, 233.98, null, 26.99, 5.0, 7.0, 9.0)
    println(filter(list))
    println(filter(list2))
}

fun filter(list: List<Double?>):Double {
    return rondToTwoSign(list
        .filterNotNull()
        .map { if (it.toInt()%2==0) {(it*it)} else { (it/2)} }
        .filter { it < 25}
        .sortedDescending()
        .take(10)
        .sum())
}

fun rondToTwoSign(sum: Double): Double{
    return (sum * 100.0).roundToInt()/100.0
}